package com.aceinteract.bergerpaintvisualizer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_select_color.*

class SelectColorActivity : AppCompatActivity() {

    private var colors = ArrayList<Color>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_color)

        loadColors()

        val colorsAdapter = ColorsRecyclerAdapter(this@SelectColorActivity, colors)
        colors_recycler!!.layoutManager = GridLayoutManager(this@SelectColorActivity, 6)
        colors_recycler!!.setHasFixedSize(true)
        colors_recycler!!.adapter = colorsAdapter

        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun loadColors() {
        val jsonString = assets.open("colors.json").bufferedReader().use { it.readText() }
        val type = object : TypeToken<ArrayList<Color>>() {}.type
        colors = Gson().fromJson(jsonString, type)
    }
}
