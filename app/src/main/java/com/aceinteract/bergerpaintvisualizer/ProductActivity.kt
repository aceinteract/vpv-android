package com.aceinteract.bergerpaintvisualizer

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_product.*

class ProductActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        val title = intent.getStringExtra("title")
        val description = intent.getStringExtra("description")
        val imageDrawable = intent.getIntExtra("imageDrawable", -1)
        val touchDryTime = intent.getStringExtra("touchDryTime")
        val hardDryTime = intent.getStringExtra("hardDryTime")
        val coverageCapacity = intent.getStringExtra("coverageCapacity")
        val areaOfUse = intent.getStringExtra("areaOfUse")

        supportActionBar!!.title = title
        text_product_description.text = description
        image_product.setImageDrawable(resources.getDrawable(imageDrawable))
        text_product_touch_dry.text = touchDryTime
        text_product_hard_dry.text = hardDryTime
        text_product_coverage_capacity.text = coverageCapacity
        text_product_area_of_use.text = areaOfUse

    }
}
