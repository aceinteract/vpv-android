package com.aceinteract.bergerpaintvisualizer

import android.graphics.Bitmap
import android.graphics.ImageFormat
import android.hardware.Camera
import android.os.Handler
import android.os.Looper
import android.view.SurfaceHolder
import android.widget.ImageView
import java.io.IOException

class CameraPreview(private val previewLayoutWidth: Int, private val previewLayoutHeight: Int, private val cameraPreview: ImageView) : SurfaceHolder.Callback, Camera.PreviewCallback {

    private val bitmap = Bitmap.createBitmap(previewLayoutWidth, previewLayoutHeight, Bitmap.Config.ARGB_8888)
    private val pixels = IntArray(previewLayoutWidth * previewLayoutHeight)

    private var imageFormat: Int = -1
    private var processing: Boolean = false
    private var frameData: ByteArray? = null

    private var camera: Camera? = null

    val handler = Handler(Looper.getMainLooper())

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        val parameters = camera!!.parameters
        parameters.setPreviewSize(previewLayoutWidth, previewLayoutHeight)

        imageFormat = parameters.previewFormat

        camera!!.parameters = parameters
        camera!!.startPreview()
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        with(camera!!) {
            setPreviewCallback(null)
            stopPreview()
            release()
        }
        camera = null
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        camera = Camera.open()

        try {
            camera!!.setPreviewDisplay(holder)
            camera!!.setPreviewCallback(this)
        } catch (exception: IOException) {
            camera!!.release()
            camera = null
        }
    }

    override fun onPreviewFrame(data: ByteArray?, camera: Camera?) {
        if (imageFormat == ImageFormat.NV21) {
            if (!processing) {
                frameData = data
                handler.post(doImageProcessing)
            }
        }
    }

    fun onPause() {
        camera!!.stopPreview()
    }

    val doImageProcessing = Runnable {
        processing = true
        imageProcessing(previewLayoutWidth, previewLayoutHeight, frameData!!, pixels)

        bitmap.setPixels(pixels, 0, previewLayoutWidth, 0, 0, previewLayoutWidth, previewLayoutHeight)
        cameraPreview.setImageBitmap(bitmap)
        processing = false
    }

    external fun imageProcessing(width: Int, height: Int, NV21FrameData: ByteArray, pixels: IntArray): Boolean

    companion object {
        init {
            System.loadLibrary("imageProcessing")
        }
    }

}