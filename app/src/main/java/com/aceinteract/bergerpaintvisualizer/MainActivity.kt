package com.aceinteract.bergerpaintvisualizer

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(this@MainActivity,
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            layout_goto_visualizer.setOnClickListener({
                startActivity(Intent(this@MainActivity, VisualizerActivity::class.java))
            })

            layout_goto_locate_stores.setOnClickListener({
                startActivity(Intent(this@MainActivity, StoresActivity::class.java))
            })

            layout_goto_find_products.setOnClickListener({
                startActivity(Intent(this@MainActivity, ProductsActivity::class.java))
            })
        } else {
            ActivityCompat.requestPermissions(this@MainActivity,
                    arrayOf(Manifest.permission.CAMERA), 1234)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == 1234) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this@MainActivity, "Access Granted", Toast.LENGTH_SHORT).show()
                layout_goto_visualizer.setOnClickListener({
                    startActivity(Intent(this@MainActivity, VisualizerActivity::class.java))
                })

                layout_goto_locate_stores.setOnClickListener({
                    startActivity(Intent(this@MainActivity, StoresActivity::class.java))
                })

                layout_goto_find_products.setOnClickListener({
                    startActivity(Intent(this@MainActivity, ProductsActivity::class.java))
                })
            } else {
                Toast.makeText(this@MainActivity, "Access Denied. Closing app", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}
