package com.aceinteract.bergerpaintvisualizer

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_visualizer.*

class VisualizerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visualizer)

        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        layout_goto_live_visualizer.setOnClickListener({
            startActivity(Intent(this@VisualizerActivity, LiveVisualizerActivity::class.java))
        })

        layout_goto_image_visualizer.setOnClickListener({
            startActivity(Intent(this@VisualizerActivity, ImageVisualizerActivity::class.java))
        })
    }
}
