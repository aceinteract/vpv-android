package com.aceinteract.bergerpaintvisualizer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_products.*

class ProductsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)

        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        layout_goto_decorative_products.setOnClickListener({
            startActivity(Intent(this@ProductsActivity, DecorativeProductsActivity::class.java))
        })

        layout_goto_non_decorative_products.setOnClickListener({
            startActivity(Intent(this@ProductsActivity, NonDecorativeProductsActivity::class.java))
        })
    }

}
