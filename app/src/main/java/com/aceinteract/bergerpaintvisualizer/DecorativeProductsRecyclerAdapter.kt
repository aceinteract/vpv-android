package com.aceinteract.bergerpaintvisualizer

import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class DecorativeProductsRecyclerAdapter(var context: Context, private var products: ArrayList<Product>) : RecyclerView.Adapter<DecorativeProductsRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_decorative_product, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titleTextView.text = products[position].title
        holder.descriptionTextView.text = products[position].description
        holder.image.setImageDrawable(context.resources.getDrawable(products[position].imageDrawable))

        holder.cardView.setOnClickListener {
            val intent = Intent(context, ProductActivity::class.java)
            intent.putExtra("title", products[position].title)
            intent.putExtra("description", products[position].description)
            intent.putExtra("imageDrawable", products[position].imageDrawable)
            intent.putExtra("touchDryTime", products[position].touchDryTime)
            intent.putExtra("hardDryTime", products[position].hardDryTime)
            intent.putExtra("areaOfUse", products[position].areaOfUse)
            intent.putExtra("coverageCapacity", products[position].coverageCapacity)
            context.startActivity(intent)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val cardView: CardView = itemView.findViewById(R.id.card_product)
        val titleTextView: TextView = itemView.findViewById(R.id.text_product_title)
        val descriptionTextView: TextView = itemView.findViewById(R.id.text_product_description)
        val image: ImageView = itemView.findViewById(R.id.image_product)

    }

}
