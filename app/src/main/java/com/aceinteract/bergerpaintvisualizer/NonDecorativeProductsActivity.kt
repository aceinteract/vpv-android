package com.aceinteract.bergerpaintvisualizer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_non_decorative_products.*

class NonDecorativeProductsActivity : AppCompatActivity() {

    private var products = ArrayList<Product>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_non_decorative_products)

        loadData()

        val artistsAdapter = ProductsRecyclerAdapter(this@NonDecorativeProductsActivity, products)
        products_recycler!!.layoutManager = LinearLayoutManager(this@NonDecorativeProductsActivity)
        products_recycler!!.setHasFixedSize(true)
        products_recycler!!.adapter = artistsAdapter

        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun loadData() {

        products.add(Product("AUTOLUX",
                "High Quality auto paints for refurbishing of automobiles and bodyframes. " +
                        "They come in three forms Autolux KH, Autoctryl and Cellulose Primer Grey.",
                null, null, null,
                null, R.drawable.ic_image_black_24dp))

        products.add(Product("BERGER PROTECTON",
                "These consist of wide range of protective paints products such as  Epoxy Coatings, " +
                        "Acrylic Polyurethane(PM7750/7756), Marine Coatings(Alkyd GlossPM 1800), " +
                        "Auto fouling Paints, Silicone Heat Resisting PM5600/5601, Bright Aluminum and Road Marking.",
                null, null, null,
                null, R.drawable.ic_image_black_24dp))

        products.add(Product("BERGERNOL",
                "Bergernol is a wood paint used for treating and preserving wood from from fungicide and insect attack. " +
                        "It is ideal fro wood surfaces that are to be painted for treatment and for rafter’s fences.",
                null, null, null,
                null, R.drawable.ic_image_black_24dp))

        products.add(Product("PAINT THINNER",
                "Paint Thinner is solvent based paint by-product used to decrease viscosity of the paints, " +
                        "It is available in decorative and non decorative forms.",
                null, null, null,
                null, R.drawable.ic_image_black_24dp))

        products.add(Product("LIGNOLAC",
                "Lignolac is our trade name for wood finishes that provides the best of finishes to give a charming appearance. " +
                        "It enhances the grain and makes the wood appear attractive.",
                null, null, null,
                null, R.drawable.ic_image_black_24dp))

    }
}
