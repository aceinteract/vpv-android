package com.aceinteract.bergerpaintvisualizer

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.Point
import android.os.Build
import android.util.Log
import android.view.Display
import android.view.WindowManager
import android.widget.ImageView
import java.lang.reflect.InvocationTargetException


class ViewUtils {

    companion object {

        fun getNavigationBarSize(context: Context): Point {
            val appUsableSize = getAppUsableScreenSize(context)
            val realScreenSize = getRealScreenSize(context)

            // navigation bar on the side
            if (appUsableSize.x < realScreenSize.x) {
                return Point(realScreenSize.x - appUsableSize.x, appUsableSize.y)
            }

            // navigation bar at the bottom
            return if (appUsableSize.y < realScreenSize.y) {
                Point(appUsableSize.x, realScreenSize.y - appUsableSize.y)
            } else Point()

            // navigation bar is not present
        }

        private fun getAppUsableScreenSize(context: Context): Point {
            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            return size
        }

        private fun getRealScreenSize(context: Context): Point {
            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = windowManager.defaultDisplay
            val size = Point()

            if (Build.VERSION.SDK_INT >= 17) {
                display.getRealSize(size)
            } else {
                try {
                    size.x = Display::class.java.getMethod("getRawWidth").invoke(display) as Int
                    size.y = Display::class.java.getMethod("getRawHeight").invoke(display) as Int
                } catch (e: IllegalAccessException) {
                } catch (e: InvocationTargetException) {
                } catch (e: NoSuchMethodException) {
                }
            }

            return size
        }

        fun scaleBitmap(bm: Bitmap, maxHeight: Int, maxWidth: Int): Bitmap {
            val scaleX = maxWidth / bm.width.toDouble()
            val scaleY = maxHeight / bm.height.toDouble()
            val scale = Math.min(scaleX, scaleY)

            Log.v("ImageVisualizerActivity", (bm.width * scale).toString())
            Log.v("ImageVisualizerActivity", (bm.height * scale).toString())

            return Bitmap.createScaledBitmap(bm, (bm.width * scale).toInt(), (bm.height * scale).toInt(), false)
        }

        fun getBitmapPositionInsideImageView(imageView: ImageView): IntArray {
            val ret = IntArray(4)

            if (imageView.drawable == null)
                return ret







            // Get image dimensions
            // Get image matrix values and place them in an array
            val f = FloatArray(9)
            imageView.imageMatrix.getValues(f)

            // Extract the scale values using the constants (if aspect ratio maintained, scaleX == scaleY)
            val scaleX = f[Matrix.MSCALE_X]
            val scaleY = f[Matrix.MSCALE_Y]

            // Get the drawable (could also get the bitmap behind the drawable and getWidth/getHeight)
            val d = imageView.drawable
            val origW = d.intrinsicWidth
            val origH = d.intrinsicHeight

            // Calculate the actual dimensions
            val actW = Math.round(origW * scaleX)
            val actH = Math.round(origH * scaleY)

            ret[2] = actW
            ret[3] = actH

            // Get image position
            // We assume that the image is centered into ImageView
            val imgViewW = imageView.width
            val imgViewH = imageView.height

            val top = (imgViewH - actH) / 2
            val left = (imgViewW - actW) / 2

            ret[0] = left
            ret[1] = top

            return ret
        }
    }

}