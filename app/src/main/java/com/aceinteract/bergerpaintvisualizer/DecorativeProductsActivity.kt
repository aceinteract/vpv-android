package com.aceinteract.bergerpaintvisualizer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_decorative_products.*

class DecorativeProductsActivity : AppCompatActivity() {

    private var products = ArrayList<Product>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_decorative_products)

        loadData()

        val artistsAdapter = DecorativeProductsRecyclerAdapter(this@DecorativeProductsActivity, products)
        products_recycler!!.layoutManager = LinearLayoutManager(this@DecorativeProductsActivity)
        products_recycler!!.setHasFixedSize(true)
        products_recycler!!.adapter = artistsAdapter

        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun loadData() {

        products.add(Product("LUXOL EMULSION - 4L & 20L",
                "Luxol is a premium water based acrylic matt emulsion, suitable for " +
                        "interior and exterior decoration. It gives a brilliant finish on the wall " +
                        "and specially fortified against fungal and mould attack leaving the walls beautiful for a long time.",
                "30 minutes", "2 hours", "Interior and Exterior",
                "10 - 12 metres square per litre", R.mipmap.product1))
        products.add(Product("LUXOL GLOSS - 4L & 20L",
                "Air drying paint with high gloss. Specially formulated to prevent mould " +
                        "and fungi growth on walls. It is suitable for internal and External decoration.",
                "6 hours", "24 hours", "Interior or Exterior",
                "10 metres square per litre", R.mipmap.product2))
        products.add(Product("SUPER STAR EMULSION - 4L & 20L",
                "Water Based Matt Finish, suitable for interior and exterior purpose. " +
                        "Specially formulated to prevent Mould  growth.",
                "18 minutes", "2 hours", "Interior or Exterior",
                "8 - 9 metres square per litre", R.mipmap.product3))
        products.add(Product("SUPATEX - 4L & 20L",
                "An affordable quality textured paints that provides a stony and fine patterned finish on walls.",
                null, null, null,
                null, R.mipmap.product4))
        products.add(Product("SUPER STAR GLOSS - 4L & 20L",
                "This is a quality and affordable air drying paint , perfect for transforming " +
                        "the outdoor and indoor space into a colourful environment.",
                "5 minutes", "18 minutes", "Interior or Exterior",
                "8 - 9 metres square per litre", R.mipmap.product3))
        products.add(Product("CLINSTAY - 20L",
                "A washable emulsion paint with satin finish(mild gloss) . Easily washable to remove dirt " +
                        "and stains. It is a high performance finish that allows for easy cleaning , light scrubbing " +
                        "and specially formulated for high traffic areas.",
                "30 minutes", "2 hours", "Interior or Exterior",
                "12 metres square per litre", R.mipmap.product5))
        products.add(Product("RUFHIDE - 20L",
                "A premium water based putty with superior adhesive strength for wall preparation, " +
                        "covering of minor cracks and holes before painting.",
                "3 hours", "8 hours", "Interior or Exterior",
                "10 metres square per litre", R.mipmap.product6))
        products.add(Product("ROBBIALAC - 4L & 20L",
                "Robialac Emulsion is an emulsion paint that provides long lasting and smooth finish on walls. " +
                        "It is very affordable, durable and highly recommended for interior purpose.",
                "18 minutes", "24 hours", "Interior or Exterior",
                "6 - 8 metres square per litre", R.mipmap.product7))
        products.add(Product("FIRE RETARDANT TEXCOTE - 20L",
                "A high quality textured paint with superior light, fast pigment. Specially formulated to prevent " +
                        "the growth of algae and fungal attack.",
                "18 minutes", "24 hours", "Interior or Exterior",
                "8 - 9 metres square per litre", R.mipmap.product9))
    }
}
