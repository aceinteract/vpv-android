package com.aceinteract.bergerpaintvisualizer

import android.app.Activity
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

class ColorsRecyclerAdapter(var context: Context, private var colors: ArrayList<Color>) : RecyclerView.Adapter<ColorsRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_color, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = colors.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.colorImageView.setImageDrawable(ColorDrawable(android.graphics.Color.parseColor("#${colors[position].hexCode}")))
        holder.colorImageView.setOnClickListener {
            VisualizerApplication.instance!!.selectedColor = android.graphics.Color.parseColor("#${colors[position].hexCode}")
            (context as Activity).finish()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val colorImageView: ImageView = itemView.findViewById(R.id.image_selected_color)
    }
}