package com.aceinteract.bergerpaintvisualizer

import android.app.Application
import android.graphics.Color

class VisualizerApplication : Application() {

    var selectedColor = Color.rgb(255, 255, 255)

    companion object {

        @get:Synchronized
        var instance: VisualizerApplication? = null
            private set

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}