package com.aceinteract.bergerpaintvisualizer

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_image_visualizer.*

class StoresActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private val stores: HashMap<String, LatLng> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stores)

        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        loadStores()
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun loadStores() {
        stores["128 Aba Owerri Road, beside Kemjika Filling Station, Abia State"] = LatLng(5.128333, 7.360562)
        stores["Laffro house, beside Hasan Furniture, Moshood Abiola way, Abeokuta"] = LatLng(7.167938, 3.372688)
        stores["748, Alenxandria Crescent, Wuse 11, Behind Barnex Plaza, F.C.T. Abuja"] = LatLng(9.0837, 7.465877)
        stores["Barrister place,Tomatoes junction Deidei FCT"] = LatLng(9.0805665, 7.4664779)
        stores["102 Oyemekun rd, opposite Oyemekun Grammar School, Akure"] = LatLng(7.263499, 5.17775)
        stores["379, Nnebisi Road. Asaba"] = LatLng(6.205473, 6.721556)
        stores["Km 20 Enugu / Onitsha Express Road, opp. Crunches Eatery, Unizik Junction, Awka"] = LatLng(6.2219079, 7.0868439)
        stores["134, Murtala Mohammed Way, Benin City, Edo State"] = LatLng(6.3280757, 5.6403479)
        stores["14, Murtala Mohammed Way, Calabar."] = LatLng(4.989819, 8.333496)
        stores["Plot 214, First Avenue State Housing Estate, Calabar"] = LatLng(4.9850992, 8.3373497)
        stores["MBM Ajitadidun Estate, Along Adebayo Road. Ado Ekiti"] = LatLng(7.6554983, 5.2296964)
        stores["33, Abakaliki Road, G.R.A, Enugu"] = LatLng(6.461811, 7.504001)
        stores["1, Azeez Aina Street, Off Ring Road, Ibadan, Oyo State"] = LatLng(7.3565553, 3.8702913)
        stores["Abu Plaza,Elebu Market road, off Akala express, Elebu Oluyole extension, Ibadan"] = LatLng(7.352486, 3.824015)
        stores["Inside Debistol oil, Olopomeji busstop, Opp wetlands hotel, Akobo, Ibadan"] = LatLng(7.4044204, 3.9441713)
        stores["Km 30, Lagos Epe Expressway Casia Estate, Abijo GRA"] = LatLng(6.4686529, 3.4923618)
        stores["4 Ifelodun Street, Off Asa Dam Road, Offa Garage, Ilorin"] = LatLng(8.4514072, 4.5806123)
        stores["No.16,Tos Benson Road (Former Beach Road), Ebute Ikorodu, Lagos"] = LatLng(6.605269, 3.3478667)
        stores["1A, Constitution Hill Road, Opposite Pirelli tyres, Joys, Plateau State"] = LatLng(9.9051133, 8.891676)
        stores["1440, Kachia Road, Kaduna, Kaduna State."] = LatLng(10.4861451, 7.4160316)
        stores["4, Ajasa Street, Off Civic Centre Road, Kano State."] = LatLng(11.9832844, 8.5429084)
        stores["19, Murtala Mohammed Way, Opposite kano Residential Hotel, Kano"] = LatLng(11.9832844, 8.5429084)
        stores["91, Allen Avenue, Ikeja Lagos"] = LatLng(6.605269, 3.3478667)
        stores["102, Oba Akran Avenue, Ikeja Industrial Estate"] = LatLng(6.6010673, 3.3364635)
        stores["Berger place, along Lagos Ibadan expressway behind Lagos state Accident & Emergency centre"] = LatLng(6.605269, 3.3478667)
        stores["Adenekan Mega Plaza, No 1 Adenekan street opposite Access Bank, Okota road, Isolo"] = LatLng(6.605269, 3.3478667)
        stores["Km 16, Lekki Epe Expressway Agungi/Osapa Bus Stop"] = LatLng(6.4686529, 3.4923618)
        stores["105, Baga Road, Maiduguri, Borno State"] = LatLng(11.902102, 13.08546)
        stores["Tonimas Plaza, by Railway Crossing, Old Otukpo Road, Benue State"] = LatLng(7.089792, 7.657574)
        stores["Beside tilley Gydao College, Makurdi -Lafia Road, North Bank, Makurdi, Benue State"] = LatLng(7.769292, 8.561354)
        stores["SM3, Ahamadu Bahago Plaza, Tunga, Minna"] = LatLng(9.603431, 6.56331)
        stores["Pentagon Plaza, Bukan Sidi, beside Investment House, Jos Road, Lafia"] = LatLng(11.0860104, 7.716289)
        stores["Akwa Road, Onitsha, Anambra State"] = LatLng(6.151028, 6.802582)
        stores["No 35 blossom Plaza, Onitsha, Owerri Road, Nnewi"] = LatLng(6.2219079, 7.0868439)
        stores["306, Ikirun road, Beside Iyana camp opp.NNPC, Ota Efun, Ikirun Road, Oshogbo"] = LatLng(7.8242904, 4.5883262)
        stores["51/65 Mbaise Road, by Wetheral/Fire Service Roundabout. Owerri"] = LatLng(5.4839214, 7.0145165)
        stores["No 42 Old Aba Road by Artillery Junction Portharcourt"] = LatLng(4.8446588, 7.0382423)
        stores["70, Ordinance Road, Trans Amadi Industrial Estate, Portharcourt"] = LatLng(4.843555, 7.0383416)
        stores["13,Ahmadu Bello way, Sokoto"] = LatLng(13.0543789, 5.1625844)
        stores["111, Warri-Effurun Road, Warri, Delta State"] = LatLng(5.586068, 5.7796854)
        stores["Km 5, Refinery Road,Warri,Delta State"] = LatLng(5.5691557, 5.7283273)
        stores["Km 2,Okpe road, Osubi behind victory joe filling station, Delta State"] = LatLng(5.586068, 5.7796854)
        stores["No 51,Ikot Ekpene Road, Uyo"] = LatLng(5.0534452, 7.8785121)
        stores["Keana house,3 Lagos crescent Garki 2 FCT Abuja"] = LatLng(9.0805665, 7.4664779)
        stores["Shop No 8, Kwakwansh Plaza, opposite Mohafiya Filling Station Mandalla Road, Suleja, Niger State"] = LatLng(9.217861, 7.189803)
        stores["Lekki Garent plaza Km 17/18,Lekki - Epe expressway, Igbo - Efon b/stop, Lekki"] = LatLng(6.4686529, 3.4923618)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Lagos and move the camera
        val lagos = LatLng(6.465422, 3.406448)
        stores.forEach {
            mMap.addMarker(MarkerOptions().position(it.value).title(it.key))
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lagos))
        mMap.moveCamera(CameraUpdateFactory.zoomTo(8f))
    }
}
