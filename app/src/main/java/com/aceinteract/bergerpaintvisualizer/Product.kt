package com.aceinteract.bergerpaintvisualizer

class Product(var title: String, var description: String, var touchDryTime: String?,
              var hardDryTime: String?, var areaOfUse: String?, var coverageCapacity: String?,
              var imageDrawable: Int)