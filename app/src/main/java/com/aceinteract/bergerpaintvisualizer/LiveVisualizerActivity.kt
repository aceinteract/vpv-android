package com.aceinteract.bergerpaintvisualizer

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.SurfaceView
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_live_visualizer.*
import org.opencv.android.*
import org.opencv.core.*
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc
import java.util.*
import org.opencv.core.Mat




class LiveVisualizerActivity : AppCompatActivity(), CameraBridgeViewBase.CvCameraViewListener2, View.OnTouchListener {

    private var cameraView : JavaCameraView? = null

    var str = "str"

    private var offsetX = 0
    private var offsetY = 0

    private var originalSize: Size = Size()

    private var points = ArrayList<Point>()
    private var points1 = ArrayList<Point>()

    private var colorTo = Color.rgb(255, 255, 255)

    private val whiteScalar = Scalar(255.0, 255.0, 255.0)
    private val whiteScalarSingle = Scalar(255.0)
    private val zeroScalar = Scalar.all(0.0)
    private val floodFillDiff = Scalar(3.0, 3.0, 3.0)

    private var mask: Mat? = null
    private var temp: Mat? = null
    private var img: Mat? = null
    private var output: Mat? = null
    private var floodFilled: Mat? = null

    private val roi = Rect(1, 1, 500, 500)

    private val tag = "LiveVisualizerActivity"

    private val mLoaderCallback = object : BaseLoaderCallback(this) {
        @SuppressLint("ClickableViewAccessibility")
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    val p = str.length
                    str = "g"
                    Log.i("LiveVisualizer", "OpenCV loaded successfully")

                    val ad = AlertDialog.Builder(this@LiveVisualizerActivity).create()
                    ad.setCancelable(false) // This blocks the 'BACK' button
                    ad.setTitle("Instructions")
                    ad.setMessage("Simply select a color with the \"Choose Color\" button and touch " +
                            "the parts of the wall you want to paint and tap the \"Reset\" button to clear all painting")
                    ad.setButton(DialogInterface.BUTTON_POSITIVE, "OK") { dialog, _ ->
                        cameraView!!.setMaxFrameSize(1920, 1080)
                        cameraView!!.enableView()
                        cameraView!!.setOnTouchListener(this@LiveVisualizerActivity)
                        dialog.dismiss()
                    }
                    ad.show()

                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.activity_live_visualizer)

        colorTo = VisualizerApplication.instance!!.selectedColor
        image_selected_color.setImageDrawable(ColorDrawable(colorTo))

        val decorView = window.decorView
        decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            or View.SYSTEM_UI_FLAG_FULLSCREEN
                            or View.SYSTEM_UI_FLAG_IMMERSIVE)
                } else {
                    decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            or View.SYSTEM_UI_FLAG_FULLSCREEN)
                }
            }
        }

        cameraView = findViewById(R.id.HelloOpenCvView)
        cameraView!!.visibility = SurfaceView.VISIBLE

        cameraView!!.setCvCameraViewListener(this)

    }

    public override fun onPause() {
        super.onPause()
        if (cameraView != null)
            cameraView!!.disableView()
    }

    public override fun onResume() {
        super.onResume()

        colorTo = VisualizerApplication.instance!!.selectedColor
        image_selected_color.setImageDrawable(ColorDrawable(colorTo))

        if (!OpenCVLoader.initDebug()) {
            Log.d(tag, "Internal OpenCV library not found. Using OpenCV Manager for initialization")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_2, this, mLoaderCallback)
        } else {
            Log.d(tag, "OpenCV library found inside package. Using it!")
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        if (cameraView != null)
            cameraView!!.disableView()
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        offsetX = ViewUtils.getNavigationBarSize(this@LiveVisualizerActivity).x / 2
        Log.v("CameraViewLog", offsetX.toString())

        originalSize = Size(width.toDouble(), height.toDouble())

        img = Mat(originalSize, CvType.CV_8UC4)
        temp = Mat()
        mask = Mat(img!!.size(), CvType.CV_8UC3)
        floodFilled = Mat.zeros(500 + 2, 500 + 2, CvType.CV_8U)
        output = Mat(img!!.size(), CvType.CV_8UC3)

        image_selected_color.setOnClickListener {
            startActivity(Intent(this@LiveVisualizerActivity, SelectColorActivity::class.java))
        }

        reset_btn.setOnClickListener {
            points = ArrayList()
            points1 = ArrayList()
        }
    }

    override fun onCameraViewStopped() {

    }


    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame?): Mat {

        Imgproc.cvtColor(inputFrame!!.rgba(), img, Imgproc.COLOR_RGBA2RGB)
        Imgproc.resize(img, img, Size(500.0, 500.0))

        floodFilled = Mat.zeros(500 + 2, 500 + 2, CvType.CV_8U)

        mask = Mat.zeros(img!!.size(), CvType.CV_8UC3)

        output = Mat.zeros(img!!.size(), CvType.CV_8UC3)

//        Imgproc.resize(output, output, Size(500.0, 500.0))
//        output!!.convertTo(output, CvType.CV_8UC3)

        output!!.setTo(Scalar(Color.red(colorTo).toDouble(), Color.green(colorTo).toDouble(), Color.blue(colorTo).toDouble()))

        try {
            for (point in points) {
                Imgproc.floodFill(img, floodFilled, point, whiteScalarSingle, null, floodFillDiff,
                        floodFillDiff, 4 + (255 shl 8) + Imgproc.FLOODFILL_MASK_ONLY)

                Core.subtract(floodFilled, zeroScalar, floodFilled)

                floodFilled!!.submat(roi).copyTo(temp)
                temp!!.copyTo(mask, temp)
            }
            Core.bitwise_not(mask, mask)
            img!!.copyTo(output, mask)

            Imgproc.resize(output, output, originalSize)
            for (point in points1) {
                Core.circle(output, point, 10, whiteScalar, 4)
                Core.circle(output, point, 10, Scalar(Color.red(colorTo).toDouble(),
                        Color.green(colorTo).toDouble(), Color.blue(colorTo).toDouble()), -1)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mask!!.release()
        output!!.release()
        temp!!.release()
        floodFilled!!.release()
        img!!.release()

        System.gc()

        return output!!

    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {

        // Fill and get the mask
        val point = Point(event!!.x.toDouble() + offsetX , event.y.toDouble() + offsetY)
        val x = (point.x / originalSize.width) * 500.0
        val y = (point.y / originalSize.height) * 500.0

        val point1 = Point(x, y)

        Log.d(tag, "seed: $point")

        points.add(point1)
        points1.add(point)

        return v!!.performClick()
    }
}
