package com.aceinteract.bergerpaintvisualizer

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_image_visualizer.*
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.Exception as JavaException


class ImageVisualizerActivity : AppCompatActivity() {

    private var bitmap: Bitmap? = null

    private var img: Mat? = null
    private var imgDisplay: Mat? = null

    private var points = ArrayList<Point>()

    private var colorTo = Color.rgb(255, 255, 255)

    private val tag = "ImageVisualizerActivity"

    private val mLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    Log.i("LiveVisualizer", "OpenCV loaded successfully")


                    val ad = AlertDialog.Builder(this@ImageVisualizerActivity).create()
                    ad.setCancelable(false) // This blocks the 'BACK' button
                    ad.setTitle("Instructions")
                    ad.setMessage("Simply select a color with the \"Choose Color\" button and touch " +
                            "the parts of the wall you want to paint and tap the \"Reset\" button to clear all painting")
                    ad.setButton(DialogInterface.BUTTON_POSITIVE, "OK") { dialog, _ ->
                        val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
                        galleryIntent.type = "image/*"
                        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE)
                        startActivityForResult(Intent.createChooser(galleryIntent, "Open Image"), 1024)
                        dialog.dismiss()
                    }
                    ad.show()

                    image_selected_color.setOnClickListener {
                        startActivity(Intent(this@ImageVisualizerActivity, SelectColorActivity::class.java))
                    }
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_visualizer)

        colorTo = VisualizerApplication.instance!!.selectedColor
        image_selected_color.setImageDrawable(ColorDrawable(colorTo))

        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        if (!OpenCVLoader.initDebug()) {
            Log.d(tag, "Internal OpenCV library not found. Using OpenCV Manager for initialization")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallback)
        } else {
            Log.d(tag, "OpenCV library found inside package. Using it!")
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

    override fun onResume() {
        super.onResume()

        colorTo = VisualizerApplication.instance!!.selectedColor
        image_selected_color.setImageDrawable(ColorDrawable(colorTo))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) =
            if (requestCode == 1024 && resultCode == Activity.RESULT_OK) {
                if (data!!.data != null) {
                    try {
                        val stream = contentResolver.openInputStream(data.data)
                        bitmap = BitmapFactory.decodeStream(stream)
                        bitmap = ViewUtils.scaleBitmap(bitmap!!, image.height, image.width)

                        img = Mat(bitmap!!.height, bitmap!!.width, CvType.CV_8UC3)
                        imgDisplay = Mat(bitmap!!.height, bitmap!!.width, CvType.CV_8UC3)

                        Utils.bitmapToMat(bitmap, img)

                        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGRA2BGR)

                        img!!.copyTo(imgDisplay)

                        updateImageView(img!!)
                        Log.v(tag, "width: " + img!!.cols().toString() + ", height: " + img!!.rows().toString())

                        image.setOnTouchListener { _, event ->
                            val x = event!!.x - ViewUtils.getBitmapPositionInsideImageView(image)[0]
                            val y = event.y - ViewUtils.getBitmapPositionInsideImageView(image)[1]

                            if (x <= img!!.cols() && y <= img!!.rows()) {
                                val point = Point(x.toDouble(), y.toDouble())
                                points.add(point)

                                when (event.action) {
                                    MotionEvent.ACTION_DOWN -> {
                                        text_painting_wall.visibility = View.VISIBLE
                                        text_painting_wall.text = getString(R.string.content_painting_wall)
                                        progress_painting.visibility = View.VISIBLE
                                        val handler = Handler()
                                        handler.postDelayed({
                                            Log.i("LiveVisualizer", "Updating image")
                                            floodFill()
                                            progress_painting.visibility = View.GONE
                                            text_painting_wall.text = getString(R.string.content_painting_done)
                                        }, 200)
//                                        Core.circle(imgDisplay, point, 15,
//                                                Scalar(255.0, 255.0, 255.0), 5)
//                                        Core.circle(imgDisplay, point, 15, Scalar(Color.red(colorTo).toDouble(),
//                                                Color.green(colorTo).toDouble(), Color.blue(colorTo).toDouble()), -1)
//                                        updateImageView(imgDisplay!!)
                                    }
                                    MotionEvent.ACTION_UP -> {

                                    }
                                    MotionEvent.ACTION_MOVE -> {

                                    }
                                }
                            }
                            true
                        }
                        stream.close()
                    } catch (e: FileNotFoundException) {
                        Toast.makeText(this@ImageVisualizerActivity, "Could not find image!", Toast.LENGTH_LONG).show()
                        finish()
                    } catch (e: IOException) {
                        Toast.makeText(this@ImageVisualizerActivity, "Could not open image!", Toast.LENGTH_LONG).show()
                        finish()
                    }
                } else {
                    finish()
                }
            } else {
                finish()
            }

    private fun updateImageView(mat: Mat) {
        Utils.matToBitmap(mat, bitmap)
        image.setImageBitmap(bitmap)
    }

    private fun floodFill() {
        val floodFilled = Mat.zeros(img!!.rows() + 2, img!!.cols() + 2, CvType.CV_8U)
        val mask = Mat(img!!.size(), CvType.CV_8UC3)

        try {
            for (point in points) {
                Imgproc.floodFill(img, floodFilled, point, Scalar(255.0), null, Scalar(3.0, 3.0, 3.0),
                        Scalar(3.0, 3.0, 3.0), 4 + (255 shl 8) + Imgproc.FLOODFILL_MASK_ONLY)

                Core.subtract(floodFilled, Scalar.all(0.0), floodFilled)

                val roi = Rect(1, 1, img!!.cols(), img!!.rows())
                val temp = Mat()

                floodFilled.submat(roi).copyTo(temp)
                temp.copyTo(mask, temp)
            }
            val output = Mat(img!!.size(), CvType.CV_8UC3)
            output.setTo(Scalar(Color.red(colorTo).toDouble(), Color.green(colorTo).toDouble(), Color.blue(colorTo).toDouble()))
            Core.bitwise_not(mask, mask)
            img!!.copyTo(output, mask)
//            for (point in points) {
//                Core.circle(output, point, 15,
//                        Scalar(255.0, 255.0, 255.0), 5)
//                Core.circle(output, point, 15, Scalar(Color.red(colorTo).toDouble(),
//                        Color.green(colorTo).toDouble(), Color.blue(colorTo).toDouble()), -1)
//            }
            updateImageView(output)
        } catch (e: JavaException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        Log.v(tag, "Updated image")

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_image_visualizer, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_load_image) {

            val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
            galleryIntent.type = "image/*"
            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(Intent.createChooser(galleryIntent, "Open Image"), 1024)

        } else if (item.itemId == R.id.action_reset) {
            points = ArrayList()
            updateImageView(img!!)
            img!!.copyTo(imgDisplay)
        }
        return super.onOptionsItemSelected(item)
    }

}
